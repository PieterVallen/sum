# TDD

### SUM
Implementation of the exercise TDD-1 of Juan Antonio.

Started from example:
- https://spring.io/guides/tutorials/spring-boot-kotlin/

Used also:
- https://phauer.com/2018/best-practices-unit-testing-kotlin/
- https://www.baeldung.com/junit-5-kotlin
