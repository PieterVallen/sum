package ing.chapter.tdd.sum

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test

class CalculateSumTest {

    @Test
    fun `Two numbers, returns the sum`() {
        assertEquals(3, calculateSum(1, 2))
    }

    @Test
    fun `Negative numbers throw an exception`() {
        val exception = assertThrows(IllegalArgumentException::class.java) {
            calculateSum(-1, 2)
        }
        assertEquals("I can only add positive numbers between 0 and 1000!", exception.message)
    }

    @Test
    fun `Numbers greater than 1000 throw an exception`() {
        val exception = assertThrows(IllegalArgumentException::class.java) {
            calculateSum(1, 1001)
        }
        assertEquals("I can only add positive numbers between 0 and 1000!", exception.message)
    }

}
