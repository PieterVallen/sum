package ing.chapter.tdd.sum

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpStatus

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class IntegrationTest(@Autowired val restTemplate: TestRestTemplate) {

    @BeforeAll
    fun setup() {
        println(">> Setup")
    }

    @Test
	fun `Two numbers, returns the sum`() {
        val body = SumRequest(number1 = 1, number2 = 2)
		val entity = restTemplate.postForEntity("/sum", body, SumResponse::class.java)

		assertThat(entity.statusCode).isEqualTo(HttpStatus.OK)
		assertThat(entity.body.toString().contains("{\"result\":3}"))
	}

    @Test
    fun `An empty object returns zero`() {
        val body = Empty()
        val entity = restTemplate.postForEntity("/sum", body, SumResponse::class.java)

        assertThat(entity.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(entity.body.toString().contains("{\"result\":0}"))
    }

    @Test
    fun `A single number returns the value`() {
        val body = SumRequest(number1 = 1)
        val entity = restTemplate.postForEntity("/sum", body, SumResponse::class.java)

        assertThat(entity.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(entity.body.toString().contains("{\"result\":1}"))
    }

    @Test
    fun `One negative number should give a 500 Error`() {
        val body = SumRequest(number1 = 1, number2 = -2)
        val entity = restTemplate.postForEntity("/sum", body, SumResponse::class.java)

        assertThat(entity.statusCode).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
        assertThat(entity.body.toString().contains("java.lang.IllegalArgumentException: I can only add positive numbers between 0 and 1000!"))
    }

    @AfterAll
    fun teardown() {
        println(">> Tear down")
    }

}