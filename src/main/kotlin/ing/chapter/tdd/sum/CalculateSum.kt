package ing.chapter.tdd.sum

data class SumRequest(val number1: Int = 0, val number2: Int = 0)

data class SumResponse(val result: Int)

fun calculateSum(number1: Int, number2: Int): Int {

    if (number1 in 0..1000 && number2 in 0..1000) {
        return number1 + number2
    } else {
        throw IllegalArgumentException("I can only add positive numbers between 0 and 1000!")
    }
}