package ing.chapter.tdd.sum

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class RestController{

    @PostMapping("/sum")
    fun sumPost(@RequestBody sumRequest: SumRequest) = SumResponse(calculateSum(sumRequest.number1, sumRequest.number2))
}
