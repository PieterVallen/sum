package ing.chapter.tdd

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SumApplication

fun main(args: Array<String>) {
	runApplication<SumApplication>(*args)
}
